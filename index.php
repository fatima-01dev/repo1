<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Todo App</title>
    <!-- Inclure les liens vers les fichiers CSS Bootstrap si nécessaire -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="#">Todo App</a>
        </div>
    </nav>

    <div class="container mt-4">
        <h1 class="mb-4">Ma Liste de tâches</h1>

        <form action="backend.php" method="post" class="mb-4">
            <div class="input-group">
                <input type="text" class="form-control" name="title" placeholder="Ajouter une nouvelle tâche" required>
                <button type="submit" class="btn btn-primary">Ajouter</button>
            </div>
        </form>

        <ul class="list-group">
            <?php foreach ($taches as $tache): ?>
                <li class="list-group-item <?php echo $tache['done'] ? 'list-group-item-success' : 'list-group-item-warning'; ?>">
                    <?php echo $tache['title']; ?>
                    <form action="backend.php" method="post" class="d-inline">
                        <input type="hidden" name="id" value="<?php echo $tache['id']; ?>">
                        <button type="submit" class="btn btn-sm btn-secondary" name="action" value="toggle">Toggle</button>
                        <button type="submit" class="btn btn-sm btn-danger" name="action" value="delete">Supprimer</button>
                    </form>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>

    <!-- Inclure les scripts JavaScript Bootstrap si nécessaire -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
